from transformers import BlipProcessor, BlipForConditionalGeneration

processor = BlipProcessor.from_pretrained("Salesforce/blip-image-captioning-base")
model = BlipForConditionalGeneration.from_pretrained("Salesforce/blip-image-captioning-base")

import socket, traceback
from PIL import Image

socket.setdefaulttimeout(5)
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversocket.bind(("127.0.0.1", 8000))
serversocket.listen()

# todo first OCR then image captioning

while True:
    try:
        print("waiting for client...")
        conn, addr = serversocket.accept()

        with conn:
            print(f"Connected by {addr}")
            fname = conn.recv(1024).strip()
            img = Image.open(fname).convert("RGB")
            
            text = "a photo of"
            inputs = processor(img, text, return_tensors="pt")
            out = model.generate(**inputs)
            
            caption = processor.decode(out[0], skip_special_tokens=True)
            caption = caption.strip()
            conn.sendall(caption.encode())
    except TimeoutError:
        pass
    except socket.timeout:
        pass
    except KeyboardInterrupt:
        break
    except:
        traceback.print_exc()

# # conditional image captioning

# out = model.generate(**inputs)
# print(processor.decode(out[0], skip_special_tokens=True))