import os, sys, re
from datetime import datetime
from zoneinfo import ZoneInfo
import requests, traceback
from disnake import Embed
from threading import Thread
import asyncio, time

import openai
import text_data
import text_data_priv
import image

entity = sys.argv[1].lower()
entity_data = text_data.entity_data[entity]
name = entity_data["name"]
uid = None # set by bot.py

ids_seen = {}
image_data = {}
avatars = {}

def getPath():
    return os.path.dirname(os.path.realpath(__file__))

openai.api_key = open(getPath() + "/openai_token").read().strip()
temperature = 1.0

if (entity == "malka"):
    temperature = 1.1

def append_template(template, messages):
    for line in template.split("\n"):
        line = line.strip()

        if (len(line) == 0):
            continue
        
        messages.append({"role": "system", "content": line})

async def inspect_image(url, id, caption_only = False):
    if (id in image_data):
        return
    try:
        img_data = requests.get(url)
        fname = getPath() + f"/tmp/images/{id}"

        fd = open(fname, "wb")
        fd.write(img_data.content)
        fd.close()
        
        if (caption_only):
            ocr = ""
        else:
            try:
                ocr = image.get_ocr(fname)
            except:
                ocr = ""
                traceback.print_exc()

        try:
            caption = await image.get_caption(fname)
        except:
            caption = ""
            traceback.print_exc()
        
        image_data[id] = {"ocr": ocr, "caption": caption}
        os.remove(fname)
    except:
        image_data[id] = None
        traceback.print_exc()

async def handle_avatars(last_msgs, header):
    # display avatars or not
    know_avatars = False

    triggers = ["avatar", "profile pic", "pfp"]

    for m in last_msgs[-10:]:
        for t in triggers:
            if (t in m.content.lower()):
                know_avatars = True
        
        if ("poza" in m.content.lower() and "profil" in m.content.lower()):
            know_avatars = True

    known = []
    avatars = []
    
    if (know_avatars):
        for m in last_msgs:
            try:
                url = m.author.display_avatar.url
                id = f"user_{m.author.id}"
                
                await inspect_image(url, id, True)
                if (image_data[id] is not None and id not in known):
                    known.append(id)
                    role = "system"

                    uname = m.author.name
                    uname = uname.replace("GPT Box", "Aklam")

                    avatars.append(f"{uname} has a profile picture with {image_data[id]['caption']}.")

            except:
                traceback.print_exc()
    
    header = header.replace("CORE_INSERT_EXTRAS", "\n".join(avatars))
    return header

async def handle_memories(last_msgs, header):
    memories = ""
    try:
        fname = getPath() + f"/memories/{name}.txt"
        f = open(fname)
        memories = f.read().strip()
        f.close()
    except:
        traceback.print_exc()

    header = header.replace("CORE_INSERT_MEMORIES", memories)
    return header

async def append_to_messages(last_msgs, messages, last_msgs_full):
    last_is_user = False
    started_talking = False

    right_now = datetime.now().replace(tzinfo=None)
    hours = None

    for msg in last_msgs:
        date_distance = right_now - msg.created_at.replace(tzinfo=None)
        date_distance = date_distance.total_seconds()
        hours_now = date_distance / 3600
        
        if (hours is not None and hours > hours_now + 1):
            hrs = int(hours - hours_now)
            messages.append({"role": "system", "content": f"* {hrs} hour{'s' if hrs > 1 else ''} later *"})

        hours = hours_now
        content = msg.content
        uname = msg.author.name
        uname = uname.replace("Pinguinul Stie-Tot", "Pinguinul") # this is ok for malka
        uname = uname.replace("GPT Box", "Aklam")

        if (msg.author.id not in text_data_priv.ids):
            text_data_priv.ids[msg.author.id] = [msg.author.name]

        for i in text_data_priv.ids:
            content = content.replace(f"<@{i}>", f"@{text_data_priv.ids[i][0]}")

        content = content.replace("<:rodinMoment:818593179774353428>", ":rodinMoment:")
        content = content.replace("<:ECSMoment:764925183161008138>", ":ECSMoment:")
        
        # print(content, len(content))
        # if (len(content) == 0):
        #     print(msg.embeds, len(msg.embeds))
        #     print(msg.attachments, len(msg.attachments))

        if (len(content) == 0 and len(msg.embeds) == 0 and len(msg.attachments) == 0 and msg.reference is None):
            continue

        if (msg.reference is not None and msg.author.name != "GPT Box"):
            ref_id = msg.reference.message_id
            
            for m in last_msgs_full:
                if (ref_id == m.id):
                    subcontent = m.content
                    if (len(subcontent) > 400):
                        subcontent = subcontent[:400] + "..."

                    replyname = m.author.name
                    replyname = replyname.replace("GPT Box", "Aklam")

                    if (len(content) == 0 and len(msg.embeds) == 0 and len(msg.attachments) == 0):
                        content = f"* pinned {replyname}'s message saying \"{subcontent}\" *"
                    elif (len(content) == 0):
                        content = f"* replies with a photo to {replyname} previously saying \"{subcontent}\" *"
                    else:
                        content = f"{content} * in reply to {replyname} previously saying \"{subcontent}\" *"

        if (msg.author.id == uid):
            last_is_user = False
            started_talking = True

            entry = f"{uname}: {content}"
            messages.append({"role": "assistant", "content": entry})
        else:
            #if (last_is_user and started_talking):
            #    messages.append({"role": "system", "content": "Pinguinul doesn't say anything"})

            last_is_user = True

            role = "user"
            entry = f"{uname}: {content}"

            if (len(content) != 0):
                messages.append({"role": role, "content": entry})
                
        embed_msgs = []

        for e in msg.embeds:
            try:
                if (e.title is None and e.description is None and e.author is None):
                    continue
                
                embed_idx = len(embed_msgs) + 1
                embed_msg = ""

                if (e.author is not None and len(e.author) > 0):
                    embed_msg += f" Author: {e.author.name}."
                if (e.title is not None and len(e.title) > 0):
                    embed_msg += f" Title: {e.title}."
                
                if (e.thumbnail is not None and e.thumbnail is not None):
                    url = e.thumbnail.proxy_url

                    if (url is not None and url is not None):
                        print("embed has an image:", url)

                        id = f"{msg.id}{embed_idx}" # TODO multiple images, disnake doesnt seem to support it

                        await inspect_image(e.thumbnail.proxy_url, id)

                        if (id in image_data and image_data[id] != None):
                            ocr = image_data[id]["ocr"]
                            caption = image_data[id]["caption"]

                            if (len(caption) != 0):
                                embed_msg += f" {name.capitalize()} thinks it shows {caption}."
                            # else:
                            #     embed_msg += " It has an image."

                            # if (len(ocr) > 0):
                            #     if (len(ocr) >= 512):
                            #         embed_msg += f" There is far too much text in the embedded image for {name.capitalize()} to read it."
                            #     else:
                            #         embed_msg += f" {name.capitalize()} can see the following text in it: {ocr}."

                if (e.description is not None and len(e.description) > 0):
                    desc = e.description

                    if (len(desc) > 400):
                        desc = desc[:400] + "..."

                    embed_msg += f" Description: {desc}."

                embed_msg = embed_msg.strip()

                if (len(embed_msg) > 0):
                    embed_msgs.append(embed_msg)
            except:
                traceback.print_exc()

        if (len(embed_msgs) == 1):
            role = "system"
            entry = f"The message above has an embed for the link: {embed_msgs[0]}"

            if (len(content) == 0):
                entry = f"{uname} sends a message with an embed: {embed_msgs[0]}"

            messages.append({"role": role, "content": entry})
        elif (len(embed_msgs) > 1):
            role = "system"
            entry = f"The message above has {len(embed_msgs)} embeds."
            
            if (len(content) == 0):
                entry = f"{uname} sends a message with {len(embed_msgs)} embeds."

            messages.append({"role": role, "content": entry})

            for i, embed_msg in enumerate(embed_msgs):
                entry = f"Embed {i + 1}: {embed_msg}"
                messages.append({"role": role, "content": entry})

        for att in msg.attachments:
            try:
                if ("image" in att.content_type):
                    if (att.size >= 8000000):
                        image_data[att.id] = None
                    else:
                        await inspect_image(att.proxy_url, att.id)
                else:
                    image_data[att.id] = None # for any attachments tbh

                if (att.id not in image_data):
                    entry = f"{uname} sent an attachment, but {name.capitalize()} hasn't looked at it yet."
                elif (image_data[att.id] is None):
                    if ("image" in att.content_type):
                        entry = f"{uname} sent an image, but {name.capitalize()} can't see it."
                    elif ("video" in att.content_type):
                        entry = f"{uname} sent a video, but {name.capitalize()} can't see it."
                    elif ("audio" in att.content_type):
                        entry = f"{uname} sent an audio file, but {name.capitalize()} can't see it."
                    elif ("text" in att.content_type):
                        entry = f"{uname} sent a text file, but {name.capitalize()} can't see it."
                    else:
                        entry = f"{uname} sent a file, but {name.capitalize()} can't see it."

                else:
                    entry = f"{uname} sent an image"
                    caption = image_data[att.id]["caption"]
                    ocr = image_data[att.id]["ocr"]

                    if (len(caption) == 0 and len(ocr) == 0):
                        entry += f", but {name.capitalize()} can't see it."
                    else:
                        if (len(caption) != 0):
                            entry += f". {name.capitalize()} thinks it's {caption}"
                        
                        if (len(ocr) != 0):
                            if (len(ocr) >= 400):
                                ocr = ocr[:400] + "..."
                                entry += f". {name.capitalize()} can see the following text: {ocr} {name.capitalize()} can't read the rest of the text"
                            else:
                                entry += f". {name.capitalize()} can see the following text: {ocr}"

                        entry += "."

                messages.append({"role": "system", "content": entry})
            except:
                traceback.print_exc()

        for r in msg.reactions:
            r_users = await r.users(limit = 3).flatten()
            r_users = [u.name for u in r_users]
            count = r.count - 3

            role = "system"
            entry = ", ".join(r_users)

            if (count > 0):
                entry += f" and {count} others"
                
            entry += f" reacted with {r.emoji} to {uname}'s message"

            messages.append({"role": role, "content": entry})

    # if (last_msg is not None):
    #     content = last_msg
    #     last_msg = None

    #     already_exists = False
    #     for msg in last_msgs[-5:]:
    #         if (msg.content == content):
    #             already_exists = True
    #             break
        
    #     if (not already_exists):
    #         for i in text_data_priv.ids:
    #             content = content.replace(f"<@{i}>", f"@{text_data_priv.ids[i][0]}")

    #         if (msg.author.id not in text_data_priv.ids):
    #             text_data_priv.ids[msg.author.id] = [msg.author.name]

    #         if (msg.author.id == uid):
    #             entry = f"{name.capitalize()}: {content}"
    #             messages.append({"role": "assistant", "content": entry})

# TODO: auto-resize output if it errors out
def get_openai_response_internal(mutable_output, messages, max_tokens, temperature):
    try:
        output = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=messages,
            temperature=temperature,
            max_tokens=max_tokens
        )

        mutable_output[0] = output
    except:
        traceback.print_exc()

async def get_openai_response(messages, max_tokens, temperature, timeout = 12):
    tries = 3
    thread_result = [None]

    while (tries > 0):
        t = Thread(target=get_openai_response_internal, args=[thread_result, messages, max_tokens, temperature])
        t.start()
        print("openai thread loop start", t.is_alive(), t)

        i = 5 * timeout
        while i > 0:
            print("thread active:", t.is_alive())
            if not t.is_alive():
                break

            await asyncio.sleep(0.2)
            i -= 1

        output = thread_result[0]
        if (output is not None):
            break

        tries -= 1
        print("tries left:", tries)

    if (output is None):
        raise Exception("OpenAI timeout!")

    output = output["choices"][0]["message"]["content"]
    output = output.strip()
    return output

async def get_message(last_msgs, history = 30, max_tokens = 350, header = entity_data["text_header"], footer = entity_data["text_footer"], raw = False, all_user = False, no_retry = False, no_memory = False):
    messages = []
    
    last_msgs_full = last_msgs
    last_msgs = last_msgs[-history:]
    
    tz_text = ""
    if (entity == "pinguin"):
        dt = datetime.now(ZoneInfo("Europe/Bucharest"))
        tz_text = " (Romanian timezone)"
    else:
        dt = datetime.now(ZoneInfo("Europe/London"))

    timestamp = dt.strftime('Today is %A, %d %B %Y. The time is %X' + tz_text + '.')

    try:
        channel_name = last_msgs[-1].channel.name
        if (entity == "pinguin"):
            chat_context = f"on the #{channel_name} channel of the HTsP Discord server"
        else:
            chat_context = f"on the #{channel_name} channel of ECS Shitposts Discord server"

    except:
        traceback.print_exc()
        chat_context = f"privately, inside DMs"

    header = header.format(timestamp = timestamp, chat_context = chat_context)
    
    header = await handle_avatars(last_msgs, header)

    if (no_memory):
        header = header.replace("CORE_INSERT_MEMORIES", "")
    else:
        header = await handle_memories(last_msgs, header)

    append_template(header, messages)
    await append_to_messages(last_msgs, messages, last_msgs_full)
    append_template(footer, messages)
    
    if (all_user):
        for msg in messages:
            msg["role"] = "user"

    print("MESSAGES")
    for msg in messages:
       print(msg)
    
    tries = 3

    while tries > 0:
        output = await get_openai_response(messages, max_tokens, temperature)
        print("output:", output)
        tries -= 1
        ok = True

        if ("language model" in output.lower() or "cannot generate" in output.lower()):
            ok = False

        # if (name in output.lower() and ("respond" in output.lower() or "response" in output.lower() or "answer" in output.lower())):
        #     # we still get some sort of "Malka cant respond like this" AI message, so we should retry
        #     ok = False

        if (ok or no_retry):
            break
        else:
            print("retrying output")
    
    for i in text_data_priv.ids:
        for alt in text_data_priv.ids[i]:
            output = output.replace(f"@{alt}", f"<@{i}>")
            output = output.replace(f"@{alt.capitalize()}", f"<@{i}>")

    # we need the spaces around, to make sure that we are not actually replacing
    # inside an already-valid emote
    output = output.replace(":rodinMoment: ", "<:rodinMoment:818593179774353428> ")
    output = output.replace(" :rodinMoment:", " <:rodinMoment:818593179774353428>")
    output = output.replace(":ECSMoment: ", "<:ECSMoment:764925183161008138> ")
    output = output.replace(" :ECSMoment:", " <:ECSMoment:764925183161008138>")

    # in case the output is actually just an emoji
    if (output == ":rodinMoment:"):
        output = "<:rodinMoment:818593179774353428>"
    
    if (output == ":ECSMoment:"):
        output = "<:ECSMoment:764925183161008138>"

    if (not raw):
        output = clean_output(output)
        
    print(f"output, filtered, raw: {raw}:", output)
    return output

def clean_output(output):
    # filter some of the notes, because they might interefere with the "language model" detection
    filters = ["(translation:", "(meaning ", "translation:", "(note ", "(note:"]

    for f in filters:
        if (("translation" in f or "meaning" in f) and name != "pinguinul"):
            continue

        if (f in output.lower()):
            output = output[:output.lower().find(f)].strip()

    # Handle "Malka: hi"
    if (output[0] == '"' and output[-1] == '"'):
        output = output[1:-1]

    filters = [f"{name}:", f"{name} : ", "pinguinul stie-tot: ", "pinguinul stie tot: ", f'"{name}": ']

    for f in filters:
        if (f in output.lower()):
            output = output[output.lower().find(f) + len(f):]

    # this is to get the message inside stuff like  "  Malka might respond like "test"  "
    if (name in output.lower()): # and ("respond" in output.lower() or "say" in output.lower() or "reply" in output.lower() or "message" in output.lower())
        if ('"' in output):
            output = output[output.find('"') + 1:]
            output = output[:output.find('"')]

    output = output.strip()

    if (output[0] == '"'):
        output = output[1:]

        if ('"' in output):
            # Handle Malka: "hi"
            if (output[-1] == '"'):
                output = output[:-1]
            else: # Handle Malka: "hi" :smile:
                pivot = output.rfind('"')
                post_text = output[pivot + 1:]
                output = output[:pivot] + post_text

    if (len(output) > 1 and output[1] == output[1].lower()):
        output = output[0].lower() + output[1:]

    if (output[-1] == "."):
        output = output[:-1]
        
    return output

async def is_curious(last_msgs, history = 6, header = entity_data["curious_text_header"], footer = entity_data["curious_text_footer"], all_user = False):
    messages = []

    last_msgs_full = last_msgs
    last_msgs = last_msgs[-history:]

    append_template(header, messages)
    await append_to_messages(last_msgs, messages, last_msgs_full)
    append_template(footer, messages)

    if (all_user):
        for msg in messages:
            if (msg["role"] != "system"):
                msg["role"] = "user"
    
    print("MESSAGES CURIOUS")
    for msg in messages:
       print(msg["content"])
    
    output = await get_openai_response(messages, max_tokens = 50, temperature = 1.0)
    output = output.strip().lower()
    
    if (len(last_msgs) >= 1):
        print(f"curious, {output} - msg: {last_msgs[-1].author.name}: {last_msgs[-1].content}")
    else:
        print(f"curious, {output}")
    
    curious = False
    
    # output is already .lower()

    if (f"{name}: ".lower() in output or "yes" in output or output.startswith("da")):
        curious = True

    if (("want" in output or "expect" in output) and "respond" in output and ("no" not in output or "doesn't" not in output or "don't" not in output)):
        curious = True

    return curious

    #return output.startswith("no") or "no indication" in output or "doesn't seem" in output # meaning we can talk

async def save_memories(last_msgs, history = 20):
    if (len(last_msgs) < history):
        return

    msg = await get_message(last_msgs, history = history, header=entity_data["memory_header"], footer=entity_data["memory_footer"], no_retry = True)
    print("memory", msg)

    last_msgs = last_msgs[-history:]

    # " " in msg is to avoid having a simple "yes" answer from GPT

    if (" " in msg):
        ok = False

        if (msg.lower().startswith("yes")):
            msg = msg[msg.find(" ") + 1:]
            ok = True

        name2 = name.capitalize()
        filters = [f"{name2} would remember that ", f"{name2} might remember that ",
                    f"{name2} would also remember that ", f"{name2} might also remember that "]
    
        # for f in filters:
        #     msg = msg.replace(f.lower(), "")
        #     msg = msg.replace(f, "")

        for f in filters:
            if (f.lower() in msg.lower()):
                ok = True
        
        if (ok):
            print("OK, making memory:", msg)
            fname = getPath() + f"/memories/{name}.txt"
            f = open(fname, "a")
            f.write(f"\n- {msg}")
            f.close()

            await compress_memories()

async def compress_memories():
    if (name == "malka"):
        prompt = [{"role": "system", "content": "Below are memories made by a fun girl called Malka:"}]
    else:
        prompt = [{"role": "system", "content": "Below are memories made by a Romanian guy called Pinguinul:"}]

    fname = getPath() + f"/memories/{name}.txt"
    f = open(fname)
    memories = f.read().split("\n")
    f.close()

    for m in memories:
        m = m.strip()
        if (len(m) == 0):
            continue

        prompt.append({"role": "system", "content": m})
    
    prompt.append({"role": "system", "content": f"Condense the above memories as a list, while still keeping memorable information such as people's preferences and quirks, {name.capitalize()}'s opinion of people and memorable events'."})

    print("memo_compress")
    for p in prompt:
        print(p)

    output = await get_openai_response(prompt, 1500, 1.1)
    print("rewritten_memories", output)

    fname = getPath() + f"/memories/{name}.txt"
    f = open(fname, "w")
    memories = f.write(output)
    f.close()