import random, requests, datetime

import text_data

headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0'}
months = {'Jan.':'Ianuarie', 'Feb.':'Februarie', 'March':'Martie', 'April':'Aprilie', 'May':'Mai', 'June':'Iunie', 'July':'Iulie', 'Aug.':'August', 'Sept.':'Septembrie', 'Oct.':'Octombrie', 'Nov.':'Noiembrie', 'Dec.':'Decembrie'}

def parse_ctf_count(message):
    cnt = [int(s) for s in message if s.isdigit ()]

    if (len (cnt) == 0):
        if (" un " in message.lower ()):
            cnt = 1
        elif (" doua " in message.lower ()):
            cnt = 2
        else:
            cnt = 3
    else:
        cnt = cnt[0]

        if (cnt > 8):
            cnt = 8
        elif (cnt < 1):
            cnt = 1

    return cnt

def get_active_ctfs():
    r = requests.get ("https://ctftime.org/", headers = headers).text.replace ('&#39;', "'")

    if (r.find ("Now running") != -1):
        outMsg = random.choice (text_data.ctfuriacum) + '\n\n'
        r = r[r.find ("Now running"):]
        r = r[r.find("<table"):r.find("</table>")]

        number = 1

        while (r.find ("<a href=") != -1):
            first = r[r.find ("<a href="):]
            link = first[9:first.find("\" style")]

            r = r[r.find ("#000000") + 1:]
            r = r[r.find ("#000000") + 9:]
            name = r[:r.find ("</a>")]

            r = r[r.find ("<td>&nbsp;") + 11:]
            timeLeft = r[:r.find (" more")]

            outMsg += text_data.ctfNowTemplate.format (**locals ())
            number += 1
    else:
        outMsg = random.choice (text_data.noctf)

    return outMsg

def get_ctfs(cnt):
    outMsg = random.choice (text_data.ctfuri) + '\n'
    r = requests.get ("https://ctftime.org/event/list/?year={}&online=-1&format=0&restrictions=-1&upcoming=true".format (datetime.datetime.now ().year), headers = headers).text.replace('&#39;', "'")
    
    for month in months:
        r = r.replace (month, months[month])

    r = r[r.find ('<table class="table table-striped">'):]
    r = r[:r.find ('</table>')]
    r = r.split ('<tr>')[2:]
    
    number = 1

    for entry in r:
        entry = entry.split('<td>')

        link = entry[1]
        name = link[link.find('">') + 2:link.find('</a>')]
        link = link[link.find('"') + 1:link.find('">')]

        date = entry[2][:-5]
        dateStart = date[:date.find ("&mdash;")].strip ()
        dateEnd = date[date.find ("&mdash;") + 7:].strip ()

        CTFType = entry[3][:-5]
        location = entry[4][:-5].strip()
        if (len (location) == 0): location = "?"
        weight = entry[5][:-5]
        notes = entry[6][:-5].strip()[3:-4]
        teams = entry[7][:-5].strip()
        teams = teams[teams.find('<b>') + 3:teams.find('</b>')]
        
        outMsg += text_data.ctfListTemplate.format (**locals ())

        if (number == cnt or number >= len (r)):
            break

        number += 1

    outMsg += "Tine minte, Ora Romaniei este UTC+2."
    print("wat")
    return outMsg