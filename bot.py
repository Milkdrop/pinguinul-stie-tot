import disnake, asyncio
import os, traceback, random
import sys, time

if (len(sys.argv) < 2 or sys.argv[1].lower() not in ['pinguin', 'malka']):
    print("Run it with the argument: 'pinguin' or 'malka'")
    exit(0)

uid = None

import audio
import text_data
import ctf
import text
from datetime import datetime
import random

entity = sys.argv[1].lower()

def getPath():
    return os.path.dirname(os.path.realpath(__file__))

entity_data = text_data.entity_data[entity]
prefix = entity_data["prefix"]
name = entity_data["name"]
version = "2.0"

client = disnake.Client(intents=disnake.Intents.all())

debug_dump = {}
voice_connections = {}
message_data = {}
pin_queued = False

@client.event
async def on_raw_reaction_add(reaction):
    global pin_queued
    
    if (entity == "pinguin"):
        try:
            if (reaction.guild_id != 433672740516397056): return

            pinEmote = "\U0001F4CC"
            message = await client.get_channel(reaction.channel_id).fetch_message(reaction.message_id)

            # user.guild_permissions.manage_messages and
            if (reaction.emoji.name == pinEmote):
                alreadyPinned = False

                for r in message.reactions:
                    if (r.emoji == pinEmote):
                        async for user in r.users():
                            if (user.id != reaction.member.id):
                                alreadyPinned = True
                                break
                
                if (alreadyPinned): return

                pinsChannel = 687237577807626273
                if (len(message.content) > 0):
                    content = f"\"{message.content}\" - <@{message.author.id}>"
                else:
                    content = f"<@{message.author.id}>"

                embed = None

                if (len(message.embeds) > 0):
                    embed = message.embeds[0]

                for a in message.attachments:
                    content += "\n" + a.url
                
                await client.get_channel (pinsChannel).send (content, embed = embed)
        except:
            traceback.print_exc()
    
    if (pin_queued):
        return

    # channel = client.get_channel(reaction.channel_id)

    # print("reaction channel", channel)

    # if (channel is not None):
    #     msg = await channel.fetch_message(reaction.message_id)
        
    #     print("reaction message", msg)

    #     if (msg is not None):
    #         channel_id = msg.channel.id
            
    #         if (msg.author.id == uid):
    #             await initialize_channel(msg.channel)
    #             print("reacted to malka msg!")

    #             limit = 2
    #             for m in message_data[channel_id]["msgs"][-3:]:
    #                 if (m.author.id == uid):
    #                     limit -= 1
                
    #             if (limit > 0):
    #                 for m in message_data[channel_id]["msgs"][-5:]:
    #                     if (m.id == msg.id):
    #                         print("malka msg is recent!")

    #                         pin_queued = True
    #                         try:
    #                             await talk_in_channel(channel, delay = 1 + random.random() * 3, talked_lookup=0)
    #                         except:
    #                             traceback.print_exc()

    #                         pin_queued = False

@client.event
async def on_ready():
    global uid

    uid = client.user.id
    text.uid = uid

    print(client.user.name + " booted up.")
    await client.change_presence(activity = disnake.Game(name = entity_data["presence"]))

async def initialize_channel(channel):
    global message_data
    channel_id = channel.id

    if (channel_id not in message_data):
        message_data[channel_id] = {}
        message_data[channel_id]["was_curious"] = 0
        message_data[channel_id]["last_msg_sent_time"] = 0
        message_data[channel_id]["message_queue"] = []
        message_data[channel_id]["last_msg_seen"] = None
        message_data[channel_id]["memorized_msgs"] = []

    msgs = await channel.history(limit=50).flatten()
    # print("debug_msgs")
    # for m in msgs:
    #     print(f"{m.author.name}: {m.content}")

    message_data[channel_id]["msgs"] = []
    
    for m in msgs[::-1]:
        if (f"{prefix} force_response" not in m.content.lower()):
            message_data[channel_id]["msgs"].append(m)
        
        if (f"{prefix} force_response" in m.content.lower()):
            message_data[channel_id]["last_msg_seen"] = None

        if (m.content.lower() == f"{prefix} reset"):
            message_data[channel_id]["msgs"] = []

async def talk_in_channel(channel, delay=0, talked_lookup = 5, debug = False):
    global message_data
    channel_id = channel.id
    
    await client.wait_until_ready()

    print("should chat, sleeping for", delay)
    if (delay > 0):
        await asyncio.sleep(delay)
    print("chattin :D")

    await initialize_channel(channel)
    
    if (talked_lookup != 0):
        for msg in message_data[channel_id]["msgs"][-talked_lookup:]:
            if (msg.author.id == uid):
                return False

    output = await text.get_message(message_data[channel_id]["msgs"], history = 10, max_tokens = 50, header = entity_data["unrequested_text_header"], footer = entity_data["unrequested_text_footer"], raw = True)
    #output = "yes"

    if (output.lower().startswith("yes") or output.lower().startswith("da") or f"{name}: " in output.lower()):
        output = await text.get_message(message_data[channel_id]["msgs"], history = 10)
        #output = output[output.lower().find(f"{name}: ") + len(f"{name}: "):].strip()

        await channel.trigger_typing()
        message_data[channel_id]["was_curious"] = 2

        print("OK:", output)
        if (not debug):
            await channel.send(output)
        
        return True
    
    return False

@client.event
async def on_message(message):
    global message_data

    if (message.author == client.user or len(message.content) == 0): return

    if (message.guild is not None):
        guild_id = message.guild.id
        in_dms = False
    else:
        in_dms = True
        guild_id = None
        return # disable DM support
        
    author_id = message.author.id
    channel_id = message.channel.id
    msg = message.content
    
    # wait in queue
    max_wait = 5 * 60 * 5
    if (channel_id in message_data):
        while (len(message_data[channel_id]["message_queue"]) > 0 and max_wait > 0):
            print("waiting in queue:", len(message_data[channel_id]["message_queue"]))
            max_wait -= 1
            await asyncio.sleep(0.2)
    
        if (max_wait == 0):
            message_data[channel_id]["message_queue"] = []
            
    try:
        await initialize_channel(message.channel)
        message_data[channel_id]["message_queue"].append(message)

        output = ""
        spell = False
        curious = False

        if (entity == "pinguin" and msg[0] == "$"):
            spell = True
            output = msg[1:]

        elif (msg.lower().startswith(prefix.lower())):
            msg_cmd = msg[len(prefix):].strip()
            cmd = msg_cmd.split(" ")[0].lower()

            if (entity == "pinguin" and cmd == "ajutor"):
                output = f":penguin: Pinguinul Stie-Tot (REBORN!) **v{version}**\n\n"

                for cmd in text_data.help_cmds:
                    output += f"\t**- {cmd.replace('prefix', prefix + ' ')}:** `{text_data.help_cmds[cmd]}`\n"

            elif (entity == "pinguin" and msg_cmd == "treci incoa"):
                output = "Nu mai e nevoie de comanda asta, poți doar să scrii $ceva și vin :)"

            elif (entity == "pinguin" and cmd == "setarivoce"):
                output = f"Comenzile pt voce sunt acum de tipul: '{prefix} voce pitch 100' sau '{prefix} voce speed 100'"

            elif (entity == "pinguin" and cmd == "voce"):
                if (len(msg_cmd.split(" ")) != 3):
                    output = f"Trebuie să scrii ceva gen: {prefix} voce pitch 100"
                else:
                    setting = msg_cmd.split(" ")[1].lower()
                    value = msg_cmd.split(" ")[2].lower()
                    
                    settings = ["pitch", "speed"]

                    if (setting in settings):
                        success = audio.change_setting(author_id, setting, value)
                        if (success):
                            output = f"Ți-am pus {setting}-ul la {value}."
                        else:
                            output = f"Nu poți seta {setting}-ul la {value}."
                    else:
                        output = f"Poți schimba următoarele opțiuni: {settings}"

            elif (entity == "pinguin" and cmd == "papa"):
                if (guild_id in voice_connections):
                    await voice_connections[guild_id].disconnect()
                    voice_connections.pop(guild_id)
                    
                output = random.choice(text_data.leaveVoice)
            
            elif (entity == "pinguin" and cmd == "ctf"):
                if ("acum" in msg_cmd.lower()):
                    output = ctf.get_active_ctfs()
                else:
                    output = ctf.get_ctfs(ctf.parse_ctf_count(msg_cmd.lower()))
            
            elif (cmd == "force_response"):
                curious = True

            elif ("shut up" in msg.lower() or "taci" in msg.lower()):
                message_data[channel_id]["was_curious"] = 0

            elif (cmd == "debug"):
                if (author_id == 184564860804136960):
                    ch_id = msg_cmd.lower().split(" ")[1]
                    ch = int(ch_id)
                    channel = await client.fetch_channel(ch)
                    await initialize_channel(channel)
                    await talk_in_channel(channel, debug = True)
                    output = "dbg"

            elif (cmd == "memodebug"):
                if (author_id == 184564860804136960):
                    ch_id = msg_cmd.lower().split(" ")[1]
                    section = msg_cmd.lower().split(" ")[2]
                    ch = int(ch_id)
                    section = int(section)
                    
                    channel = await client.fetch_channel(ch)

                    debug_dump[ch] = await channel.history(limit=200).flatten()
                    debug_dump[ch] = debug_dump[ch][::-1]

                    if (section > 0):
                        msgs = debug_dump[ch][(-section - 20):(-section)]
                    else:
                        msgs = debug_dump[ch][(-section - 20):]

                    await text.save_memories(msgs, history = 20)
                    output = " "

        # stop watching chat after 30 mins
        if (int(time.time()) >= message_data[channel_id]["last_msg_sent_time"] + 30 * 60):
            message_data[channel_id]["was_curious"] = 0

        if (len(output) == 0 and len(message_data[channel_id]["msgs"]) > 0):
            try:
                # dont send 2 messages one after the other
                ok = True

                # if (message_data[channel_id]["msgs"][-1].author.id == uid):
                #     ok = False
                #     print("skipping msg cuz we already talked")
                
                last_msg_seen = message_data[channel_id]["msgs"][-1].id
                if (message_data[channel_id]["last_msg_seen"] == last_msg_seen):
                    ok = False
                    print("race condition caught!")
                message_data[channel_id]["last_msg_seen"] = last_msg_seen

                already_talked = False
                
                if (ok):
                    if (curious == False): # and
                        mentioning = False
                        if (message.reference is not None):
                            ref = message.reference
                            found = False
                            for m in message_data[channel_id]["msgs"]:
                                if (m.id == ref.message_id):
                                    found = True
                                    if (m.author.id == uid):
                                        mentioning = True

                            if (found == False):
                                subchannel = await client.fetch_channel(ref.channel_id)
                                submsg = await subchannel.fetch_message(ref.message_id)
                                message_data[channel_id]["msgs"] = [submsg] + message_data[channel_id]["msgs"]
                                if (submsg.author.id == uid):
                                    mentioning = True

                        if (message_data[channel_id]["was_curious"] > 0 or mentioning or in_dms):
                            footer = entity_data["curious_text_footer"]
                            # we already talked
                            if (message_data[channel_id]["msgs"][-1].author.id == uid):
                                print("already talked, so changing prompt")
                                already_talked = True
                                curious = True
                            else:
                                curious = await text.is_curious(message_data[channel_id]["msgs"], history = 6, footer = footer, all_user = True)
                                
                        else:
                            forbidden_channels = [
                                778667413998796810 # vent
                            ]

                            forbidden_categories = [
                                1068238054483034226,
                                894602219528278056,
                                764590604268208199
                            ]

                            forbidden_guilds = [
                                433672740516397056
                            ]
                            
                            try:
                                category_id = message.channel.category.id
                            except:
                                category_id = None
                            
                            is_called = False

                            for c in entity_data["callers"]:
                                if c in msg.lower():
                                    is_called = True

                            if (f"<@{uid}>" in msg.lower()):
                                is_called = True

                            if (is_called):
                                curious = await text.is_curious(message_data[channel_id]["msgs"], history = 6)

                            # elif (random.random() >= 0.98
                            #     and channel_id not in forbidden_channels
                            #     and category_id not in forbidden_categories
                            #     and guild_id not in forbidden_guilds):

                            #     delay = 10 + random.random() * 8
                            #     asyncio.create_task(talk_in_channel(message.channel, delay))
                    
                    if (curious == False):
                        if (message_data[channel_id]["was_curious"] > 0):
                            message_data[channel_id]["was_curious"] -= 1
                    else:
                        history = 20
                        has_memory = False

                        # handle memory
                        for m in message_data[channel_id]["msgs"][-history:]:
                            if (m.id in message_data[channel_id]["memorized_msgs"]):
                                has_memory = True
                        
                        print("has_memory", has_memory)
                        if (not has_memory):
                            for m in message_data[channel_id]["msgs"][-history:]:
                                message_data[channel_id]["memorized_msgs"].append(m.id)
                            
                            if (len(message_data[channel_id]["memorized_msgs"]) >= history * 3):
                                message_data[channel_id]["memorized_msgs"] = message_data[channel_id]["memorized_msgs"][history:]

                            asyncio.create_task(text.save_memories(message_data[channel_id]["msgs"], history = history))

                        message_data[channel_id]["was_curious"] = 3

                        if (already_talked):
                            footer = entity_data["text_footer_already_answered"]
                            bot_msg = message_data[channel_id]["msgs"][-1].content
                            #footer = footer.format(chat_response = bot_msg) # not needed anymore
                            
                            # remove latest bot msg
                            output = await text.get_message(message_data[channel_id]["msgs"], history = 6, raw = True, footer = footer, no_retry = True)

                            ok = False
                            if ('"' in output or f"{name}:" in output.lower()):
                                output = text.clean_output(output)
                                ok = True
                                
                            if (not ok):
                                output = ""
                        else:
                            if (message.channel is not None):
                                await message.channel.trigger_typing()

                            output = await text.get_message(message_data[channel_id]["msgs"], history = history)
                    
            except:
                traceback.print_exc()

        output = output.strip()
        
        if (len(output) > 0):
            message_data[channel_id]["last_msg_sent_time"] = int(time.time())

            if (spell):
                try:
                    try:
                        voice_channel = client.get_channel(message.author.voice.channel.id)
                    except:
                        voice_channel = None
                    
                    if (voice_channel != None):
                        if (guild_id in voice_connections and not voice_connections[guild_id].is_connected()):
                            await voice_connections[guild_id].disconnect()
                            voice_connections.pop(guild_id)
                        
                        if (guild_id in voice_connections and voice_connections[guild_id].channel.id != voice_channel.id):
                            await voice_connections[guild_id].disconnect()
                            voice_connections.pop(guild_id)

                        if (guild_id not in voice_connections):
                            voice_client = await voice_channel.connect()
                            print("Connecting to channel")
                            voice_connections[guild_id] = voice_client

                        voice_client = voice_connections[guild_id]

                        if (voice_client.is_connected()):
                            voice_file = getPath() + "/audio_output/{}.wav".format(guild_id)
                            audio.generate_audio(output, author_id, voice_file)

                            voice_client.stop()
                            audioSource = disnake.FFmpegPCMAudio(voice_file)
                            voice_client.play(audioSource)
                except:
                    traceback.print_exc()
            
            await message.channel.send(output)

            # wait til message is readable
            delay = 0.5
            for i in range(3):
                await asyncio.sleep(delay)
                msgs = await message.channel.history(limit=5).flatten()

                ok = False
                for m in msgs:
                    if (m.content == output):
                        ok = True
                
                if (ok):
                    break

                delay += 0.5
    except:
        traceback.print_exc()

    try:
        message_data[channel_id]["message_queue"].remove(message)
    except:
        pass

async def browse_around():
    global message_data

    await client.wait_until_ready()

    while True:
        await asyncio.sleep(6 * 60 * 60)
        try:
            print("browsing around...")

            # map with guild data
            browsing_spaces = {
                # 433672740516397056: {
                #     "channel_ids": [519222188885409793, 516472824546852886, 610862473838329886, 611849353887350795, 692755811495051264],
                #     "categories_ok": [585924995742433312] #539447589657903114
                # },

                992243672370389022: {
                    "channel_ids": [992243672370389025, 1050748931031699456, 1071926434311970886],
                    "categories_ok": [992244798268375090]
                },

                760098399869206529: {
                    "channel_ids": [785671339864096768, 762763878672629790, 807683245764378654, 768587403627331584],
                    "categories_ok": [764813605954125854, 762719349231976499, 770700811915100180]
                }
            }

            channels = []

            for gid in browsing_spaces:
                try:
                    guild = await client.fetch_guild(gid)
                    print(guild)
                    all_channels = await guild.fetch_channels()

                    channel_ids = browsing_spaces[gid]["channel_ids"]
                    categories_ok = browsing_spaces[gid]["categories_ok"]

                    for c in all_channels:
                        if (c.type != disnake.ChannelType.text):
                            continue

                        if (c.id in channel_ids):
                            channels.append(c)
                    
                        if (c.category_id is not None and c.category_id in categories_ok):
                            channels.append(c)

                except disnake.errors.NotFound:
                    pass
                except:
                    traceback.print_exc()

            random.shuffle(channels)
            right_now = datetime.now().replace(tzinfo=None)

            max_talk_num = 1
            max_talk = {}

            for gid in browsing_spaces:
                max_talk[gid] = max_talk_num

            for c in channels:
                try:
                    if (c.guild.id not in max_talk):
                        print("Unknown channel guild", c.guild.id)
                        continue
                    
                    if (max_talk[c.guild.id] <= 0):
                        continue

                    messages = await c.history(limit=20).flatten()
                    messages = messages[::-1]

                    if (len(messages) == 0):
                        print(f"{c.name} empty?")

                    last_msg = messages[-1]

                    date_distance = right_now - last_msg.created_at.replace(tzinfo=None)
                    date_distance = date_distance.total_seconds()
                    hours = date_distance / 3600

                    print(f"last_msg ({int(hours)}h ago):", c.name, last_msg.content)

                    if (hours <= 4):
                        talked = await talk_in_channel(c)
                        print("talked in", c.name, talked)

                        if (talked):
                            max_talk[c.guild.id] -= 1

                except disnake.errors.Forbidden:
                    print("couldnt read", c.name)
                except:
                    print("error in", c.name)
                    traceback.print_exc()
        except:
            traceback.print_exc()

dirs = ["/audio_output", "/tmp", "/tmp/images", "/memories"]

for d in dirs:
    if (not os.path.isdir(getPath() + d)):
        os.mkdir(getPath() + d)

# deactivate browse_around
# client.loop.create_task(browse_around())

token_file = "token"

if (entity == "malka"):
    token_file = "token_malka"

client.run(open(getPath () + "/" + token_file).read().strip())