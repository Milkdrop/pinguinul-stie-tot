import subprocess, os, traceback

def getPath():
    return os.path.dirname(os.path.realpath(__file__))

default_voice_profile = """name pinguin
language ro
pitch {} {}
roughness 6
voicing 100
speed {}
"""

settings = {}

def generate_audio(message, author_id, output_file):
    settings_now = {"pitch": 150, "speed": 120}

    if (author_id in settings):
        for s in settings[author_id]:
            settings_now[s] = settings[author_id][s]

    voiceProfile = getPath() + "/espeak-data/voices/pinguin"
    o = open(voiceProfile, "w")
    o.write(default_voice_profile.format(settings_now["pitch"], settings_now["pitch"] + 100, settings_now["speed"]))
    o.close()

    # NEED quotes around message, otherwise when message is -f/etc/passwd it's LFI!
    subprocess.call(['espeak', '--path=' + getPath(), '-vpinguin', '-w', output_file, '"' + message + '"'])

def change_setting(author_id, setting, value):
    global settings

    try:
        value = int(value)

        if (value > 0):
            if author_id not in settings:
                settings[author_id] = {}
            settings[author_id][setting] = value
            return True
    except:
        traceback.print_exc()
    
    return False