import socket, traceback, subprocess
import asyncio

def getPath():
    return os.path.dirname(os.path.realpath(__file__))

def get_ocr(fname):
    try:
        result = subprocess.run(['tesseract', fname, "stdout"], capture_output=True, text=True)
        out_raw = result.stdout.strip()
        out = ""
        for line in out_raw.split("\n"):
            line = line.strip()
            if (len(line) == 0):
                continue
            
            out += line + "\n"

        return out
    except:
        return ""
        traceback.print_exc()

async def get_caption(fname):
    try:
        reader, writer = await asyncio.open_connection('127.0.0.1', 8000)

        writer.write(fname.encode())
        await writer.drain()

        data = await reader.read(250)

        #print("received", data)

        writer.close()
        await writer.wait_closed()

        caption = data.decode().strip()
        words = caption.split(" ")
        words_copy = words.copy()

        # remove duplicate words
        for i in range(len(words_copy) - 1, 1, -1):
            if (words_copy[i] == words_copy[i - 1]):
                del words[i]

        return " ".join(words)
    except:
        traceback.print_exc()
        return ""