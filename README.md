# Pinguinul / Malka

Hi! These are two chatbots which have *very* distinct personalities.

OpenAI token goes in an `openai_token` file, and the Discord token goes in either `token` (for Pinguinul) or `token_malka` (for Malka)

Run one or the other by using `python bot.py malka` or `python bot.py pinguin`.

Pinguinul has a bunch of extra functionalities like being a TTS or displaying information for CTFs, you may ignore all that.

# Some features:

- They can handle **many participants** in the chat at once. They can even reply to multiple people in only one message! The most I've seen is 3 at once, all with their own separate topics.

- Both bots can make **persistent memories** of the chat! They tend to remember how people treated them, preferences or events. This functionality is still under development and it's not too *impressive*, but it's not too bad either.

- **Social awareness!** The bots figure out when you want to talk on their own, you don't have to keep saying their name or use a command in order to keep a conversation going. They will figure out on their own when they don't need to send a message, be it when you are talking with someone else, reacting with a simple "lol" or just someone telling them to shut up. It's all being figured out by GPT!

- They very rarely roam around on their own and chime into conversations unprompted, which is sometimes okay and sometimes pretty annoying. I'll need to find a better way to go about that.

# For image functionality ("multimodal" stuff!):

#### To install:

`pip install transformers torch`

`sudo apt install tesseract-ocr libtesseract-dev`

#### To start the image recognition server for Malka and Pinguinul to use:

`python image_recognition/server.py`